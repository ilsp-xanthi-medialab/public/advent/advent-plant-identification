from flask import Flask, render_template, request, jsonify, redirect, url_for
import base64
import requests
import json
import numpy as np


app = Flask(__name__, static_url_path='')

@app.route("/")
def index():
    # serve the main html document
    return render_template("index.html")

@app.route('/postmethod', methods=['POST'])
def postmethod():
    print("Incoming data... forwarding to tf serving..")
    # Receive post request from client. 
    # Message data in json format
    data = request.get_json()
    tojson = json.dumps(data)

    # Forward data to localhost tfserving REST API for classifying the input image
    tfserving_response = requests.post('http://localhost:8501/v1/models/nasnet_test:predict', data=tojson)

    # Parse response from tfserving REST API as text
    response_text = json.loads(tfserving_response.text)
    print("Response from tf serving:",response_text)
    
    # Parse response from tfserving REST API as json
    response_json = tfserving_response.json()
    print("sending tf serving response to client:",response_json)
    
    # Propability Distributions
    preds = np.array(response_json['predictions'])

    # Get the max propability
    argmax_pred = np.argmax(preds)
    print("ARGMAX:",str(argmax_pred))

    # Create json object for responding to client
    toClientJSON = {'predictions': response_json['predictions'], 'argmax': int(argmax_pred) }

    # Sending response to client
    return jsonify(toClientJSON)


if __name__ == "__main__":
    # app.run(host='0.0.0.0', port=1994 ,debug=True)
    app.run(host='0.0.0.0', port=1994 ,debug=True, ssl_context="adhoc")