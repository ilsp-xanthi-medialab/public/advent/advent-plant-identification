// The buttons to start & stop stream and to capture the image
var btnInput = document.getElementById('btn-input');
var fileInput = document.getElementById('file-input');
var btnStart = document.getElementById( "btn-start" );
var btnStop = document.getElementById( "btn-stop" );
var btnCapture = document.getElementById( "btn-capture" );
var btnClassify = document.getElementById('btn-classify');
var status_text = document.getElementById("tfserving-status");

// The stream & capture
var stream = document.getElementById( "stream" );
var capture = document.getElementById( "capture" );
var snapshot = document.getElementById( "snapshot" );

// The video stream
var cameraStream = null;

// Attach listeners
btnStart.addEventListener( "click", startStreaming );
btnStop.addEventListener( "click", stopStreaming );
btnCapture.addEventListener( "click", captureSnapshot );
btnClassify.addEventListener("click",toFlask);
btnInput.addEventListener('click', () => {
    fileInput.click()
})
fileInput.addEventListener("change", loadFile);

// Function for rendering the image file when loaded from the filesystem
function loadFile(event) {
    // console.log(event.target.files[0]);
    previewImage(event.target.files[0]);
    status_text.innerHTML = "";

};


// Start Streaming
function startStreaming() {

	var mediaSupport = 'mediaDevices' in navigator;

	if( mediaSupport && null == cameraStream ) {
        navigator.mediaDevices.getUserMedia( { video:{facingMode: 'environment'}} )
        .then( function( mediaStream ) {

			cameraStream = mediaStream;

			stream.srcObject = mediaStream;

			stream.play();
		})
		.catch( function(err) {

			console.log("Unable to access camera: " + err);
		});
	}
	else {

		alert('Your browser does not support media devices.');

		return;
	}
}

// Stop Streaming
function stopStreaming() {

	if( null != cameraStream ) {

		var track = cameraStream.getTracks()[ 0 ];

		track.stop();
		stream.load();

		cameraStream = null;
	}
}

// Capture image from video stream
function captureSnapshot() {
    status_text.innerHTML = "";

	if( null != cameraStream ) {

		var ctx = capture.getContext('2d');
        ctx.drawImage( stream, 0, 0, capture.width, capture.height );
        window.tensor = preprocessInference(capture);

	}
}


var tensor;

// Function for rendering loaded image from the filesystem
function previewImage(data) {

    var ctx = capture.getContext( '2d' );
	var img = new Image();

    if(data.type.match('image.*')) {

        var reader = new FileReader();

        // Read in the image file as a data URL.
        reader.readAsDataURL(data);
        
        reader.onload = function(evt){
            if( evt.target.readyState == FileReader.DONE) {
                
                img.src = evt.target.result;
                
                img.onload = () => {

                    ctx.drawImage(img, 0, 0, capture.width, capture.height);
                
                    window.tensor = preprocessInference(img);
                }
            }
        }    

    } else {
        alert("Error! Please select an image file.");
    }

}


// Create a tensor from the image data
function preprocessInference(imgdata){

    var toTensor = tf.browser.fromPixels(imgdata)
    .resizeNearestNeighbor([224, 224])
    .toFloat();

    toTensor = toTensor.div(255);

    toTensor = toTensor.expandDims();

    console.log("Normalized tensor:",toTensor);
    toTensor.print();

    return toTensor;
}


// Function for sending post request to flask server with image data as a tensor
function toFlask(){
    status_text.innerHTML = "Processing image..";

    tensor_array = window.tensor.arraySync();

    jsonObject = {
        "instances": tensor_array
    };
    
    $.ajax({
        type: "POST",
        url: "/postmethod",
        contentType: "application/json",
        data: JSON.stringify(jsonObject),
        dataType: "json",
        success: function(response) {
            // console.log(response);
            status_text.innerHTML = "Predicted Class: "+ response.argmax
        },
        error: function(err) {  
            console.log(err);
            status_text.innerHTML = "Error... " + err;
        }
    });

}
