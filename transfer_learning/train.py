import tensorflow as tf
import tensorflow.keras.backend as K
from tensorflow.keras import regularizers
from tensorflow.keras.applications import InceptionV3,VGG16,ResNet50,MobileNetV2, NASNetMobile
from tensorflow.keras.applications import NASNetLarge, InceptionResNetV2, DenseNet121
from tensorflow.keras.applications import EfficientNetB0, EfficientNetB4
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras import layers as lay

from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.callbacks import ModelCheckpoint, CSVLogger, TensorBoard
from tensorflow.keras.optimizers import SGD, Adam
from tensorflow.keras.regularizers import l2
from tensorflow import keras
import numpy as np

import os
import matplotlib.pyplot as plt
import pathlib
import time
import json


# learning rate scheduler
def schedule(epoch):
    if epoch < 10:
        new_lr = .001
    elif epoch < 14:
        new_lr = .0006
    elif epoch < 17:
        new_lr = .0003
    elif epoch < 20:
        new_lr = .0001
    elif epoch < 23:
        new_lr = .00005
    else:
        new_lr = .00001
    
    print("\nLR at epoch {} = {}  \n".format(epoch,new_lr))
    return new_lr


def _train(train_data_dir, validation_data_dir, use_the_model, batch_size, epoch_num):
    K.clear_session()
    print("Tensorflow Version: {}".format(tf.__version__))
    print("Keras Version: {}".format(keras.__version__))

    # preprocess data
    img_width, img_height = 224, 224

    train_data_dir = train_data_dir
    validation_data_dir = validation_data_dir
    batch_size = batch_size
    use_the_model = use_the_model
    epoch_num = epoch_num

    class_dict_path = "./checkpoints/class_indices_dict.npz"
    
    specific_classes = os.listdir(train_data_dir) #None
    

    train_datagen = ImageDataGenerator(
        rescale=1. / 255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)

    test_datagen = ImageDataGenerator(rescale=1. / 255)

    train_generator = train_datagen.flow_from_directory(
        classes = specific_classes,
        directory = train_data_dir,
        target_size=(img_height, img_width),
        batch_size=batch_size,
        class_mode='categorical')

    validation_generator = test_datagen.flow_from_directory(
        classes = specific_classes,
        directory = validation_data_dir,
        target_size=(img_height, img_width),
        batch_size=batch_size,
        class_mode='categorical')

    nb_train_samples = train_generator.n
    nb_validation_samples = validation_generator.n
    n_classes = train_generator.num_classes


    # save class indices and names dictionary
    np.savez(class_dict_path , class_indices = train_generator.class_indices)


    # Select pretrained model
    model_name = ''

    if use_the_model is 1:
        base_model = InceptionV3(weights='imagenet', include_top=False)
        model_name = 'InceptionV3'
        
    elif use_the_model is 2: 
        base_model = VGG16(weights='imagenet', include_top=False)
        model_name = 'VGG16'
        
    elif use_the_model is 3: 
        base_model = ResNet50(weights='imagenet', include_top=False)
        model_name = 'ResNet50'
        
    elif use_the_model is 4: 
        base_model = InceptionResNetV2(weights='imagenet', include_top=False)
        model_name = 'InceptionResNetV2'
        
    elif use_the_model is 5: 
        base_model = NASNetMobile(input_shape=(224,224,3), weights='imagenet', include_top=False)
        model_name = 'NASNetMobile'

    elif use_the_model is 6: 
        base_model = NASNetLarge(input_shape=(331,331,3), weights='imagenet', include_top=False)
        model_name = 'NASNetLarge'
        
    elif use_the_model is 7: 
        base_model = MobileNetV2(weights='imagenet', include_top=False)
        model_name = 'MobileNetV2'
        
    elif use_the_model is 8: 
        base_model = DenseNet121(weights='imagenet', include_top=False)
        model_name = 'DenseNet121'

    elif use_the_model is 9:
        base_model = EfficientNetB0(input_shape=(224, 224,3), weights='imagenet', include_top=False)
        model_name = 'EfficientNetB0'

    elif use_the_model is 10:
        base_model = EfficientNetB4(input_shape=(224, 224,3), weights='imagenet', include_top=False)
        model_name = 'EfficientNetB4'
    
    else:
        print("Model index does not exist! Please select between 1-10.")

    print("({}) {} model loaded with {} epochs.".format(model_name,use_the_model, epoch_num))


    # Add new top layers 
    x = base_model.output
    x = lay.GlobalAveragePooling2D()(x)
    x = lay.Dense(512,activation='relu')(x)
    x = lay.Dropout(0.2)(x)

    predictions = lay.Dense(n_classes,
                        kernel_regularizer=regularizers.l2(0.005), 
                        activation='softmax')(x)

    model = Model(inputs=base_model.input, outputs=predictions)


    optimizer = Adam(lr=0.0001)

    model.compile(optimizer=optimizer, 
                loss='categorical_crossentropy', 
                metrics=['accuracy','top_k_categorical_accuracy'])


    # Add csv logger callback
    csv_filename = "./csv_logs/"+model_name+"_training_log.csv"
    csv_logger = tf.keras.callbacks.CSVLogger(csv_filename, separator=',', append=True)

    lr_scheduler = tf.keras.callbacks.LearningRateScheduler(schedule)

    checkpoint_filepath = "./checkpoints/"+model_name+"_cp-{epoch:02d}"

    checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
        filepath=checkpoint_filepath,
        save_weights_only = False,
        monitor='val_acc',
        mode='auto',
        period=5,
        save_best_only=False)


    tensorboard = TensorBoard(log_dir='./tb_logs/{}'.format(time.time()), histogram_freq = 1)

    model.fit_generator(train_generator,
                        steps_per_epoch = nb_train_samples // batch_size,
                        validation_data = validation_generator,
                        validation_steps = nb_validation_samples // batch_size,
                        epochs = epoch_num,
                        verbose = 1,
                        callbacks = [csv_logger, lr_scheduler, checkpoint_callback, tensorboard]
                        )


if __name__ == "__main__":

    with open('./train_params.json') as json_file:
        args = json.load(json_file)
        print(args)

    train_data_dir = args["train_data_dir"]
    validation_data_dir = args["validation_data_dir"]
    use_the_model = batch_size = args["use_model_num"]
    batch_size = args["b_size"]
    epoch_num = args["epochs"]


    _train(train_data_dir, validation_data_dir, use_the_model, batch_size, epoch_num)